<?php

$width = 128;
$height = 128;
$data = array();

function generate_random_integers($width, $height) {
	$data = array();
	for ($x = 0; $x < $width; $x++) {
		$html = file_get_contents('https://www.random.org/integers/?num='.$height.'&min=0&max=128&col=1&base=10&format=plain&rnd=new');
		$random_integers = explode("\n", $html);

		for($y = 0; $y < $height; $y++) {
			$data[$x][$y] = (int)$random_integers[$y];
		}
	}
	return $data;
}

function create_image($data, $width, $height) {
	header("Content-type: image/png");
	$im = imagecreate($width, $height);

	$black = imagecolorallocate($im, 0, 0, 0);
	imagefill($im, 0, 0, $black);

	for($x = 0; $x < $width; $x++){
		for($y = 0; $y < $height; $y++) {
			imagecolorallocate($im, $data[$x][$y], $data[$x][$y], $data[$x][$y]);
			$red = $data[$x][$y] & 0xFF;
			$green = $data[$x][$y] & 0xFF;
			$blue = $data[$x][$y] & 0xFF;
			$rgb = ($red << 16) | ($green << 8) | $blue;
			imagesetpixel($im, $x, $y, $rgb);
		}
	}

	imagepng($im);
	imagedestroy($im);
}

$data = generate_random_integers($width, $height);
create_image($data, $width, $height);